using System;
using System.IO;
using System.Drawing;

namespace AtariEnhanceImage
{
	class MainClass
	{
		private struct EnhancedColorInfoStruct
		{
			public Color tColor1;
			public Color tColor2;
			public Color tMixedColor;

			public int tIndex1;
			public int tIndex2;
		}

		public static void Main(string[] args)
		{
			// Create source palette.

			Bitmap tSourceBitmap = new Bitmap(args[0]);

			int tMaxDelta = 64;

			Color[] tSourcePalette = new Color[16];

			int tSourceColorCounter = 0;

			for(int tPixelY = 0; tPixelY < tSourceBitmap.Height; tPixelY++)
			{
				for(int tPixelX = 0; tPixelX < tSourceBitmap.Width; tPixelX++)
				{
					int tIndex;

					Color tPixel = tSourceBitmap.GetPixel(tPixelX, tPixelY);

					for(tIndex = 0; tIndex < tSourcePalette.Length; tIndex++)
					{
						if(tSourcePalette[tIndex].IsEmpty)
						{
							tSourcePalette[tIndex] = tPixel;
							tSourceColorCounter++;

							break;
						}
						else if(tSourcePalette[tIndex] == tPixel)
						{
							break;
						}
					}
				}
			}

			Array.Sort(tSourcePalette, delegate(Color tColor1, Color tColor2){return (int )((tColor1.R * 0.21 + tColor1.G * 0.71 + tColor1.B * 0.07) - (tColor2.R * 0.21 + tColor2.G * 0.71 + tColor2.B * 0.07));});

			// Create enhanced color info table.

			EnhancedColorInfoStruct[] tEnhancedColorInfoTable = new EnhancedColorInfoStruct[256];

			for(int tIndex = 0; tIndex < tEnhancedColorInfoTable.Length; tIndex++)
			{
				tEnhancedColorInfoTable[tIndex].tColor1 = Color.FromArgb(255, 255, 255);
				tEnhancedColorInfoTable[tIndex].tColor2 = Color.FromArgb(255, 255, 255);
				tEnhancedColorInfoTable[tIndex].tMixedColor = Color.FromArgb(255, 255, 255);

				tEnhancedColorInfoTable[tIndex].tIndex1 = -1;
				tEnhancedColorInfoTable[tIndex].tIndex2 = -1;
			}

			int tEnhancedColorIndex = 0;

			for(int tIndex1 = 0; tIndex1 < tSourceColorCounter; tIndex1++)
			{
				for(int tIndex2 = tIndex1; tIndex2 < tSourceColorCounter; tIndex2++)
				{
					tEnhancedColorInfoTable[tEnhancedColorIndex].tIndex1 = tIndex1;
					tEnhancedColorInfoTable[tEnhancedColorIndex].tIndex2 = tIndex2;

					tEnhancedColorInfoTable[tEnhancedColorIndex].tColor1 = tSourcePalette[tIndex1];
					tEnhancedColorInfoTable[tEnhancedColorIndex].tColor2 = tSourcePalette[tIndex2];

					double tR1 = tSourcePalette[tIndex1].R;
					double tG1 = tSourcePalette[tIndex1].G;
					double tB1 = tSourcePalette[tIndex1].B;

					double tR2 = tSourcePalette[tIndex2].R;
					double tG2 = tSourcePalette[tIndex2].G;
					double tB2 = tSourcePalette[tIndex2].B;

					double tGray1 = tR1 * 0.21 + tG1 * 0.71 + tB1 * 0.07;
					double tGray2 = tR2 * 0.21 + tG2 * 0.71 + tB2 * 0.07;

					if(Math.Abs(tGray1 - tGray2) <= tMaxDelta)
						tEnhancedColorInfoTable[tEnhancedColorIndex].tMixedColor = Color.FromArgb((int)((tR1 + tR2) / 2 + 0.5), (int)((tG1 + tG2) / 2 + 0.5), (int)((tB1 + tB2) / 2 + 0.5));

					tEnhancedColorIndex++;
				}
			}

			// Read enhanced image.

			Bitmap tEnhancedBitmap = new Bitmap(args[1]);

			// Create two Atari images.

			Bitmap tAtari1Bitmap = new Bitmap(320, 200, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			Bitmap tAtari2Bitmap = new Bitmap(320, 200, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

			int[,] tPi1ColorIndex1Table = new int[320, 200];
			int[,] tPi1ColorIndex2Table = new int[320, 200];

			for(int tPixelY = 0; tPixelY < tEnhancedBitmap.Height; tPixelY++)
			{
				for(int tPixelX = 0; tPixelX < tEnhancedBitmap.Width; tPixelX++)
				{
					int tIndex;

					Color tPixel = tEnhancedBitmap.GetPixel(tPixelX, tPixelY);

					for(tIndex = 0; tIndex < tEnhancedColorInfoTable.Length; tIndex++)
					{
						if(tEnhancedColorInfoTable[tIndex].tMixedColor == tPixel)
						{
							if(((tPixelY & 2) ^ (tPixelX & 2)) != 0)
							{
								tAtari1Bitmap.SetPixel(tPixelX, tPixelY, tEnhancedColorInfoTable[tIndex].tColor1);
								tAtari2Bitmap.SetPixel(tPixelX, tPixelY, tEnhancedColorInfoTable[tIndex].tColor2);

								tPi1ColorIndex1Table[tPixelX, tPixelY] = tEnhancedColorInfoTable[tIndex].tIndex1;
								tPi1ColorIndex2Table[tPixelX, tPixelY] = tEnhancedColorInfoTable[tIndex].tIndex2;
							}
							else
							{
								tAtari1Bitmap.SetPixel(tPixelX, tPixelY, tEnhancedColorInfoTable[tIndex].tColor2);
								tAtari2Bitmap.SetPixel(tPixelX, tPixelY, tEnhancedColorInfoTable[tIndex].tColor1);

								tPi1ColorIndex1Table[tPixelX, tPixelY] = tEnhancedColorInfoTable[tIndex].tIndex2;
								tPi1ColorIndex2Table[tPixelX, tPixelY] = tEnhancedColorInfoTable[tIndex].tIndex1;
							}

							break;
						}
					}
				}
			}

			tAtari1Bitmap.Save("1.png");
			tAtari2Bitmap.Save("2.png");

			// Save Pi1 images.

			// PI1 #1.

			ushort[] tAtari1Image = new ushort[1 + 16 + 320 / 16 * 4 * 200];

			tAtari1Image[0] = 0;

			for(int tIndex = 0; tIndex < 16; tIndex++)
				tAtari1Image[1 + tIndex] = (ushort )(
					(((tSourcePalette[tIndex].R & 0xe0) >> 5) << 8) | (((tSourcePalette[tIndex].R & 0x10) >> 1) << 8) |
					(((tSourcePalette[tIndex].G & 0xe0) >> 5) << 4) | (((tSourcePalette[tIndex].G & 0x10) >> 1) << 4) |
					((tSourcePalette[tIndex].B & 0xe0) >> 5) | ((tSourcePalette[tIndex].B & 0x10) >> 1));

			// for(int tIndex = 0; tIndex < 16; tIndex++)
			//	Console.WriteLine(tAtari1Image[1 + tIndex].ToString("X4") + " " + tSourcePalette[tIndex].R.ToString("X2") + "," + tSourcePalette[tIndex].G.ToString("X2") + "," + tSourcePalette[tIndex].B.ToString("X2"));

			int tDataIndex = 1 + 16;

			for(int tLineIndex = 0; tLineIndex < 200; tLineIndex++)
			{
				for(int tWordIndex = 0; tWordIndex < 320 / 16; tWordIndex++)
				{
					for(int tBitmapIndex = 0; tBitmapIndex < 4; tBitmapIndex++)
					{
						ushort tWord = 0;

						for(int tPixelIndex = 0; tPixelIndex < 16; tPixelIndex++)
							if((tPi1ColorIndex1Table[tWordIndex * 16 + tPixelIndex, tLineIndex] & (1 << tBitmapIndex)) != 0)
								tWord |= (ushort )(1 << (15 - tPixelIndex));

						tAtari1Image[tDataIndex++] = tWord;
					}
				}
			}

			// PI1 #2.

			ushort[] tAtari2Image = new ushort[1 + 16 + 320 / 16 * 4 * 200];

			tAtari2Image[0] = 0;

			for(int tIndex = 0; tIndex < 16; tIndex++)
				tAtari2Image[1 + tIndex] = (ushort )(
					(((tSourcePalette[tIndex].R & 0xe0) >> 5) << 8) | (((tSourcePalette[tIndex].R & 0x10) >> 1) << 8) |
					(((tSourcePalette[tIndex].G & 0xe0) >> 5) << 4) | (((tSourcePalette[tIndex].G & 0x10) >> 1) << 4) |
					((tSourcePalette[tIndex].B & 0xe0) >> 5) | ((tSourcePalette[tIndex].B & 0x10) >> 1));

			tDataIndex = 1 + 16;

			for(int tLineIndex = 0; tLineIndex < 200; tLineIndex++)
			{
				for(int tWordIndex = 0; tWordIndex < 320 / 16; tWordIndex++)
				{
					for(int tBitmapIndex = 0; tBitmapIndex < 4; tBitmapIndex++)
					{
						ushort tWord = 0;

						for(int tPixelIndex = 0; tPixelIndex < 16; tPixelIndex++)
							if((tPi1ColorIndex2Table[tWordIndex * 16 + tPixelIndex, tLineIndex] & (1 << tBitmapIndex)) != 0)
								tWord |= (ushort )(1 << (15 - tPixelIndex));

						tAtari2Image[tDataIndex++] = tWord;
					}
				}
			}

			// Save Pi1 images.

			byte[] tAtariPi1Image = new byte[tAtari1Image.Length * 2];

			for(int tIndex = 0; tIndex < tAtari1Image.Length; tIndex++)
			{
				tAtariPi1Image[tIndex * 2] = (byte)(tAtari1Image[tIndex] >> 8);
				tAtariPi1Image[tIndex * 2 + 1] = (byte)(tAtari1Image[tIndex] & 0xff);
			}

			File.WriteAllBytes("1.pi1", tAtariPi1Image);

			for(int tIndex = 0; tIndex < tAtari2Image.Length; tIndex++)
			{
				tAtariPi1Image[tIndex * 2] = (byte)(tAtari2Image[tIndex] >> 8);
				tAtariPi1Image[tIndex * 2 + 1] = (byte)(tAtari2Image[tIndex] & 0xff);
			}

			File.WriteAllBytes("2.pi1", tAtariPi1Image);
		}
	}
}
